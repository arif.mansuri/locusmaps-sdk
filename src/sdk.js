import observable from './observable.js'

const LIB_NAME = 'Atrius Maps JS SDK Client'
const CLIENT_VERSION = '1.1.2'

let CODE_HOME = 'https://maps.locuslabs.com/sdk/dist/current/'

let map = null
let msgId = 1 // unique IDs for each message sent to server

const sentMsgs = { } // track any sent messages to pair up to Promise
let showLogFlag = false

// logging setup
const pad2 = x => x.toString().length < 2 ? "0" + x : x
const ts = () => { const d = new Date(); return `${d.getHours()}:${pad2(d.getMinutes())}:${pad2(d.getSeconds())}.${d.getMilliseconds()}` }
const logPre = () => [`%cLocusMaps SDK %c(${ts()})%c: `, 'color: orange', 'font-size: 8px', 'font-size: inherit; color: inherit']
const log = function () {
  if (showLogFlag)
    console.log.apply(null, logPre().concat(Array.from(arguments)))
}

function installComs () {
  window.addEventListener('message', msgHandler, false)

  // the map object posts command messages and sets up a responder
  map = (payload, arg2) => {
    if (typeof payload === 'string')
      payload = { ...arg2, command: payload }
    const ob = {
      payload,
      type: 'LL-client',
      msgId: msgId++
    }
    log('Sending command object: ', payload)
    window.postMessage(ob, '*')
    return new Promise((resolve, reject) => {
      sentMsgs[ob.msgId] = { resolve, reject }
    })
  }

  observable(map) // make map handle events pub/sub
}

// this function handles any messages, possibly pairs them up with a sender or triggers an event
function msgHandler (e) {
  const d = e.data
  if (d && d.type === 'LL-server') { // confirm message is from our "server"
    if (d.clientMsgId) {
      if (d.error)
        log('Received error message', d.payload)
      else
        log('Received message', d.payload)

      if (sentMsgs[d.clientMsgId]) {
        if (d.error)
          sentMsgs[d.clientMsgId].reject(d.payload)
        else
          sentMsgs[d.clientMsgId].resolve(d.payload)
      }
    } else
    if (d.event)
      map.fire(d.event, d.payload)
  }
}

// define publicpath for any additional module fetches and insert script for initial main.js
function bootstrapCode (fn) {
  window.LLpublicPath = CODE_HOME
  const scriptNode = document.createElement('script')
  scriptNode.setAttribute('src', CODE_HOME + 'main.js')
  document.head.appendChild(scriptNode)
  scriptNode.addEventListener('load', fn)
}

// this iterates through available commands and makes them member functions of map
function addCommands (ctl, commands) {
  commands.forEach(sig => {
    ctl[sig.command] = function () {
      const cob = { command: sig.command }
      for (let i = 0; i < arguments.length; i++)
        cob[sig.args[i].name] = arguments[i]
      return ctl(cob)
    }
  })
  return ctl
}

let mapReadyObservable = null

// loads the code bundle, sets up coms, and defines a map object for map control
function newMap (renderDiv, config, codeHomeOverride) {

  const initMap = () => window.postMessage({ type: 'LL-INIT', config: { ...config, renderDiv } }, '*')

  if (codeHomeOverride)
    CODE_HOME = codeHomeOverride

  return new Promise((resolve, reject) => {
    if (!map) {
      installComs()
      if(config)
        bootstrapCode(initMap)
      mapReadyObservable = map.on('ready', (eName, data) => {
        const { commands/*, customTypes */ } = data.commandJSON

        try {
          map = addCommands(map, commands)
        } catch (e) { reject(e) }

        log('map ready')
        map.observe(function () { const ea = Array.from(arguments); ea.unshift('Event: '); log.apply(null, ea) })
        map.setLogging = LMInit.setLogging
        resolve(map)
      })
    } else {
      mapReadyObservable.detach()
      initMap()
      mapReadyObservable = map.on('ready', () => {
        resolve(map)
      })
    }
  })
}

const LMInit = {
  getVersion: () => CLIENT_VERSION,
  newMap,
  setLogging: flag => { showLogFlag = flag; if(flag) log(`${LIB_NAME} Logging enabled.`) }
}

export default LMInit
