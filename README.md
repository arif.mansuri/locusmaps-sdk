# locusmaps-sdk

This project contains a small script which can be used to initialize and control a LocusMaps instance. It is sometimes referred to as the "SDK Client".

The emphasis here is that this script remains extremely small (about 2k gzipped) - and does not change very often. Also, the resources required to display a map are not downloaded until and unless you request a map to be shown. So if there are conditions in which a map may not shown, there is very little penalty to have loaded or included *this* script into your web page.

If you do initialize a map on the page, a resource bundle is downloaded which contains all the map rendering and controlling code. This resource bundle is loaded from LocusLabs servers - it is referred to as the "SDK Server Bundle".

## Usage

There are several ways to get the SDK client incorporated into your web site or web application. Using a package manager, such as `npm` or `yarn` - or by simply adding a script tag to your web page.

### Installation via npm / yarn

The SDK client is registered with npmjs as `locusmaps-sdk`, so you may install via:

```bash
npm install locusmaps-sdk
# or
yarn add locusmaps-sdk
```

You can then use in either ES6 module style (import) or CJS module style (require).

#### ES6 Module Style

```js
import LMInit from 'locusmaps-sdk'
```

#### CJS Module Style

```js
const LMInit = require('locusmaps-sdk')
```

### Using LMInit

Then, when you are ready to initialize the map with a given `config`:

```js
LMInit.newMap('#map', config)
  .then(mapReady)
  .catch(e => console.error('Error initializing map: ', e))

function mapReady(map) {
	// Your map is ready to use here!
}
```

### Using Directly in your HTML

If you don't use a bundler, we have also made the script available via UMD (Universal Module Definition) and ES6 bundles to be injected directly to inject into your webpage.

#### UMD

The UMD bundle is compatible with [Require.js](https://requirejs.org/docs/whyamd.html) or as a standalone `<script>` insertion:

##### [Require.js](https://requirejs.org/docs/whyamd.html) (also known as AMD)

To use with RequireJS, simply refer to the URL of the UMD in your require call:

```js
require(['https://maps.locuslabs.com/sdk/LocusMapsSDK-umd.js'], LMInit => {
	const config = { /* ... */ }
	LMInit.newMap('#map', config)
	  .then(mapReady)
	  .catch(e => console.error('Error initializing map: ', e))
 })

function mapReady(map) {
	// Your map is ready to use here!
}
```

##### Directly on page (ES5)

This approach simply injects the UMD script directly on the page, which exposes the `LMInit` global variable:

```html
<head>
	<script src="https://maps.locuslabs.com/sdk/LocusMapsSDK-umd.js"></script>
	<script>
		const config = { /* ... */ }
		LMInit.newMap('#map', config)
			.then(mapReady)
			.catch(e => console.error('Error initializing map: ', e))

		function mapReady(map) {
			// Your map is ready to use here!
		}
	</script>
</head>
```

### [ES6 modules](https://hacks.mozilla.org/2015/08/es6-in-depth-modules/)

If you are embracing the most recent module format, the ES6 Module format, then you can use the ES6 module build directly as well:

```html
	<head>
		<script type="module">

			import LMInit from 'https://maps.locuslabs.com/sdk/LocusMapsSDK.js'

			const config = { /* ... */ }

			LMInit.setLogging(true)

			function go() {
			LMInit.newMap('#map', config)
				.then(start)
				.catch(e => console.error('Error initializing map: ', e))
			}

			function start(map) {
				// Your map is ready to use here!
			}

		</script>
	</head>
```
